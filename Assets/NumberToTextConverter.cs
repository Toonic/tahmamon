﻿using UnityEngine;
using System.Collections;
using System;

public static class NumberToTextConverter {

    private static string[] illionsArr = { "", " Million", " Billion", " Trillion", " Quadrillion", " Quintillion", " Sextillion", " Septillion", " Octillion", " Nonillion", " Decillion", " Undecillion", " Duodecillion", " Tredecillion", " Quattuordecillion", " Quindecillion", " Sexdecillion", " Septendecillion", " Octodecillion", " Novemdecillion", " Vigintillion", " Unvigintillion", " Duovigintillion", " Tresvigintillion", " Quattuorvigintillion", " Quinvigintillion", " Sexvigintillion", " Septenvigintillion", " Octovigintillion", " Novemvigintillion", " Trigintillion", " Untrigintillion", " Duotrigintillion", " Tretrigintillion", " Quattuortrigintillion", " Quintrigintillion", " Sextrigintillion", " Septentrigintillion", " Octotrigintillion", " Novemtrigintillion", " Quadragintillion", " Unquadragintillion", " Duoquadragintillion", " Trequadragintillion", " Quattuorquadragintillion", " Quinquadragintillion", " Sexquadragintillion", " Septquadragintillion", " Octoquadragintillion", " Novemquadragintillion", " Quinquagintillion", " Unquinquagintillion", " Duoquinquagintillion", " Trequinquagintillion", " Quattuorquinquagintillion", " Quinquinquagintillion", " Sexquinquagintillion", " Septquinquagintillion", " Octoquinquagintillion", " Novemquinquagintillion", " Sexagintillion", " Unsexagintillion", " Duosexagintillion", " Tresexagintillion", " Quattuorsexagintillion", " Quinsexagintillion", " Sexsexagintillion", " Septsexagintillion", " Octosexagintillion", " Novemsexagintillion", " Septuagintillion", " Unseptuagintillion", " Duoseptuagintillion", " Treseptuagintillion", " Quattuorseptuagintillion", " Quinseptuagintillion", " Sexseptuagintillion", " Septseptuagintillion", " Octoseptuagintillion", " Novemseptuagintillion", " Octogintillion", " Unoctogintillion", " Duooctogintillion", " Treoctogintillion", " Quattuoroctogintillion", " Quinoctogintillion", " Sexoctogintillion", " Septoctogintillion", " Octooctogintillion", " Novemoctogintillion", " Nonagintillion", " Unnonagintillion", " Duononagintillion", " Trenonagintillion", " Quattuornonagintillion", " Quinnonagintillion", " Sexnonagintillion", " Septnonagintillion", " Onctononagintillion", " Novemnonagintillion", " Centillion", " Uncentillion" };

    public static string ConvertToString(float _float) {

        if (_float.ToString().ToLower().Contains("e")) {
            int e = 6;
            int mCount = 0;

            while (_float >= float.Parse("1e+" + e)) {
                e += 3;
                mCount++;
            }

            string[] Split = _float.ToString("N0").Split(',');

            string Formated = Split[0] + "." + Split[1] + " " + illionsArr[mCount];

            return Formated;
        }
        else {
            return _float.ToString("N2");
        }
    }

}
