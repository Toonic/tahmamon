﻿using UnityEngine;
using System.Collections;

public class EvoMon : MonoBehaviour {

    public static EvoMon mon;

    public int SelectedMon = 0;

    public float[] EvoEXPLevels;

    public int CurrentMonLevel;

    private tk2dSprite MonSprite;

    public float CurrentNeededEXP = -1;

    void Awake() {

        mon = this;

        MonSprite = GetComponent<tk2dSprite>();

        if (PlayerPrefs.HasKey("SelectedMon")) {
            SelectedMon = PlayerPrefs.GetInt("SelectedMon");
        }
        else {
            SelectedMon = Random.Range(0, 8);
            PlayerPrefs.SetInt("SelectedMon", SelectedMon);
        }

        if (PlayerPrefs.HasKey("CurrentMonLevel")) {
            CurrentMonLevel = PlayerPrefs.GetInt("CurrentMonLevel");
        }
        else {
            PlayerPrefs.SetInt("CurrentMonLevel", CurrentMonLevel);
        }
    }

    void Start() {
        SetSprite();
    }

    public void SetSprite() {

        PlayerPrefs.SetInt("CurrentMonLevel", CurrentMonLevel);

        int SpriteValue = SelectedMon;

        for(int x = 0; x < EvoEXPLevels.Length; x++) {
            if(EvolutionEXP.exp.EvoEXP >= EvoEXPLevels[x] && CurrentMonLevel >= x) {
                SpriteValue += 8;
            }
            else {
                if (CurrentMonLevel < EvoEXPLevels.Length)
                    CurrentNeededEXP = EvoEXPLevels[CurrentMonLevel + 1];
                break;
            }
        }

        string SpriteName = MonSprite.CurrentSprite.name;
        SpriteName = SpriteName.Split('/')[0] + "/" + SpriteValue;

        MonSprite.SetSprite(SpriteName);

        if(MonSprite.spriteId == 0) {
            MonSprite.spriteId++;
            MonSprite.spriteId--;
        }
        else {
            MonSprite.spriteId--;
            MonSprite.spriteId++;
        }
    }


    [ContextMenu("DEV-Random Mon")]
    private void RandomizeSelectedMon() {
        SelectedMon = Random.Range(0, 7);
        PlayerPrefs.SetInt("SelectedMon", SelectedMon);
        SetSprite();
    }
}
