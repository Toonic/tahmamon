﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using TMPro;

public class AchievementMessage : MonoBehaviour {

    //@TODO - Create a Que system for visualization. This will allow me to Spam buy and still view all the achievements.

    public static AchievementMessage achievementMessage;
    public GameObject Holder;
    public TextMeshPro Message;

    public float Speed;

    void Awake() {
        achievementMessage = this;
    }

    public void GiveAchievement(string _Message) {
        Holder.transform.localPosition = new Vector3(0, -1, 0);
        Message.text = _Message;
        StartCoroutine(AnimateAchievement());
    }

    private IEnumerator AnimateAchievement() {
        Holder.transform.DOLocalMoveY(0, Speed);

        yield return new WaitForSeconds(Speed + 2f);

        Holder.transform.DOLocalMoveY(-1, Speed);
    }
}
