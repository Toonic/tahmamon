﻿using UnityEngine;
using System.Collections;
using TMPro;

public class EvolutionMenu : MonoBehaviour {

    public static EvolutionMenu evoMenu;

    public EvolutionMenuUI UI;
    
    void Awake() {
        evoMenu = this;
    }

    void Update() {
        UpdateProgressBar();
    }

    public void OpenEvoMenu() {
        if (UI.Holder.activeInHierarchy) {
            UI.Holder.SetActive(false);
        }
        else {
            UI.Holder.SetActive(true);
        }
    }

    private void UpdateProgressBar() {
        float CurrentValue = EvolutionEXP.exp.EvoEXP / EvoMon.mon.CurrentNeededEXP;
        UI.ProgressBar.Value = CurrentValue;

        UI.ProgressCurrentEXP.text = NumberToTextConverter.ConvertToString(EvolutionEXP.exp.EvoEXP) + " / " + NumberToTextConverter.ConvertToString(EvoMon.mon.CurrentNeededEXP);
    }

    public void Evolve() {
        Debug.Log("Evolve Started");
        EvolutionEXP.exp.Evolve();
    }
}

[System.Serializable]
public class EvolutionMenuUI {
    public GameObject Holder;
    public tk2dUIProgressBar ProgressBar;
    public TextMeshPro ProgressCurrentEXP;
}
