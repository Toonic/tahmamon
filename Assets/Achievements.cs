﻿using UnityEngine;
using System.Collections;

public class Achievements : MonoBehaviour {

    public static Achievements achievements;

    public bool GlobalAchievement = false; //EG: Owning 10 of All things
    public float GlobalAchievementMultiplyer = 0f;

    public Achievement[] GlobalAchievements;

    private Idler[] Idlers;

    void Awake() {
        achievements = this;
        Idlers = FindObjectsOfType<Idler>();
    }

    void Start() {
        Idlers = FindObjectsOfType<Idler>();
    }

    public void CheckGlobalAchievements(bool _VisualizeAchievement) {

        int LowestOwned = Idlers[0].NumberOwned;

        for(int x = 0; x < Idlers.Length; x++) {
            if(Idlers[x].NumberOwned <= LowestOwned) {
                LowestOwned = Idlers[x].NumberOwned;
            }
        }

        GlobalAchievementMultiplyer = 0;

        for (int x = 0; x < GlobalAchievements.Length; x++) {
            bool AchievementChecked = false;
            if (!GlobalAchievements[x].Completed)
                AchievementChecked = true;

            GlobalAchievementMultiplyer += GlobalAchievements[x].CheckAchievement(LowestOwned);

            if (AchievementChecked && GlobalAchievements[x].Completed && _VisualizeAchievement) {
                AchievementMessage.achievementMessage.GiveAchievement(GlobalAchievements[x].AchievementName);
            }
        }

        if (GlobalAchievementMultiplyer == 0)
            GlobalAchievementMultiplyer = 1;
    }

    public void Reset() {
        for (int x = 0; x < GlobalAchievements.Length; x++) {
            GlobalAchievements[x].Completed = false;
        }
        CheckGlobalAchievements(false);
    }
}

[System.Serializable]
public class Achievement {
    public string AchievementName;
    public int OwnedRequirement;
    public bool Completed;
    public float MultiplierReward; //Future we may want this to be something else.

    public float CheckAchievement(int _NumberOwned) {
        if(Completed || _NumberOwned >= OwnedRequirement) {
            Completed = true;
            return MultiplierReward;
        }
        else {
            Completed = false;
            return 0;
        }
    }
}
