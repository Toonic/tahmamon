﻿using UnityEngine;
using System.Collections;

public class EvolutionEXP : MonoBehaviour {

    public static EvolutionEXP exp;

    public float EvoEXP;
    public float EvoEXPMultiplier;

    public float LifeTimeEXP;

    void Awake() {
        exp = this;

        if (PlayerPrefs.HasKey("LifeTimeEXP")) {
            LifeTimeEXP = float.Parse(PlayerPrefs.GetString("LifeTimeEXP"));
        }
        else {
            LifeTimeEXP = 0;
        }

        EvoEXP = 300f * (Mathf.Sqrt(LifeTimeEXP / 10000000000000f));

        if (PlayerPrefs.HasKey("EvoEXPMultiplier")) {
            EvoEXPMultiplier = float.Parse(PlayerPrefs.GetString("EvoEXPMultiplier"));
        }
        else {
            EvoEXPMultiplier = 1;
        }
    }

    public void AddToLifeTimeEXP(float _EXP) {
        LifeTimeEXP += _EXP;
        PlayerPrefs.SetString("LifeTimeEXP", LifeTimeEXP.ToString());

        EvoEXP = 300f * (Mathf.Sqrt(LifeTimeEXP / 10000000000000f));
    }

    [ContextMenu("Evolve")]
    public void Evolve() {
        Idler[] Idlers = FindObjectsOfType<Idler>();

        for(int x = 0; x < Idlers.Length; x++) {
            Idlers[x].Reset();
        }

        Monster.activeMonster.Reset();
        EXP.Reset();

        Achievements.achievements.Reset();

        EvoEXPMultiplier = 1f + (0.05f * EvoEXP);

        if (EvoMon.mon.CurrentMonLevel < EvoMon.mon.EvoEXPLevels.Length) {
            if (EvoEXP >= EvoMon.mon.EvoEXPLevels[EvoMon.mon.CurrentMonLevel + 1]) {
                EvoMon.mon.CurrentMonLevel++;
                EvoMon.mon.SetSprite();
            }
        }

        if (PlayerPrefs.HasKey("EvoEXPMultiplier")) {
            if (float.Parse(PlayerPrefs.GetString("EvoEXPMultiplier")) < EvoEXPMultiplier) {
                PlayerPrefs.SetString("EvoEXPMultiplier", EvoEXPMultiplier.ToString());
            }
            else {
                EvoEXPMultiplier = float.Parse(PlayerPrefs.GetString("EvoEXPMultiplier"));
            }
        }
        else {
            PlayerPrefs.SetString("EvoEXPMultiplier", EvoEXPMultiplier.ToString());
        }
    }
}
