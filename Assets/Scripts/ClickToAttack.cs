﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ClickToAttack : MonoBehaviour {
    
    public string Name;
    public float BaseCost;
    public float Multiplier = 1.07f; //Set somewhere between 1.07 and 1.15;
    public int NumberOwned = 1;
    public float BaseIncomeRate; //Earned Per Second

    [Header("Public Stats")]
    public float Price;
    public float IncomeRate; //Earned per second.
    public float AmmountEarned = 0;
    
    [Header("Taps Per Second")]
    private List<float> taps = new List<float> ();
     public float tapsPerSecond;
    
    void Update(){
        for (int i = 0; i < taps.Count; i++) {
             if (taps[i] <= Time.timeSinceLevelLoad-1) {
                 taps.RemoveAt(i);
             }
         }
     tapsPerSecond = taps.Count;
    }
    
    void OnMouseDown(){
        taps.Add(Time.timeSinceLevelLoad);
        Price = BaseCost * Mathf.Pow(Multiplier, NumberOwned);
        IncomeRate = BaseIncomeRate * (NumberOwned);
        //EXP.AddEXP(IncomeRate);
        Monster.activeMonster.DealDamage(IncomeRate);
    }
    
}
