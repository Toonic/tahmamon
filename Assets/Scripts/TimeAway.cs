﻿using UnityEngine;
using System.Collections;
using System;

public class TimeAway : MonoBehaviour {

    public DateTime CurrentTime;
    public DateTime LastClosedTime;

    public static double LengthAway;

    private bool PauseHappened = false;

    //Load time from Playerprefs and calculate time away in Miliseconds.
    void Start() {
        CalculateTimeAway();
    }

    //Calculates the time away in seconds.
    private void CalculateTimeAway() {
        if (PlayerPrefs.HasKey("LogoutTime")) {
            long temp = Convert.ToInt64(PlayerPrefs.GetString("LogoutTime"));
            LastClosedTime = DateTime.FromBinary(temp);
        }
        else {
            LastClosedTime = DateTime.Now;
        }

        CurrentTime = DateTime.Now;

        LengthAway = (CurrentTime - LastClosedTime).TotalSeconds;

        Debug.Log("Time Away: " + LengthAway);

        if (PauseHappened)
            Monster.activeMonster.CalculateAwayEXPEarned();
    }

    //Save the time to player prefs
    void OnApplicationQuit() {
        PlayerPrefs.SetString("LogoutTime", System.DateTime.Now.ToBinary().ToString());
    }

    //Save the time to player prefs
    void OnApplicationPause(bool pauseStatus) {

        Debug.Log("Pause Status: " + pauseStatus);

        if (pauseStatus) {
            PlayerPrefs.SetString("LogoutTime", System.DateTime.Now.ToBinary().ToString());
            PauseHappened = true;
            Time.timeScale = 0;
        }
        else {
            if (PauseHappened) {
                Time.timeScale = 1;
                CalculateTimeAway();
                PauseHappened = false;
            }
        }
    }

    void OnApplicationFocus(bool pauseStatus) {
        Debug.Log("Pause Status: " + pauseStatus);
        if (!pauseStatus) {
            PlayerPrefs.SetString("LogoutTime", System.DateTime.Now.ToBinary().ToString());
            PauseHappened = true;
            Time.timeScale = 0;
        }
        else {
            if (PauseHappened) {
                Time.timeScale = 1;
                CalculateTimeAway();
                PauseHappened = false;
            }
        }
    }
}
