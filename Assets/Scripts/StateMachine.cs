﻿using UnityEngine;
using System.Collections;

public enum GameState{
    Active,
    Paused
} 

public enum BuyState{
    Buy1,
    Buy10,
    Buy100,
    BuyMax
    
}

public static class StateMachine {
    
    //Public Current list of Current States
    public static GameState gameState = GameState.Active;
    public static BuyState buyState = BuyState.Buy1;
    
    //Events
    public delegate void BuyStateChanged(BuyState _NewState);
    public static event BuyStateChanged OnBuyStateChanged;
    public delegate void GameStateChanged(GameState _NewState);
    public static event GameStateChanged OnGameStateChange;
    
    
    //Public Functions
    public static void ChangeBuyState(BuyState _NewState) { 
        buyState = _NewState;
        if(OnBuyStateChanged != null)
                OnBuyStateChanged(_NewState);
                
    }
    public static void ChangeGameState(GameState _NewState) { 
        gameState = _NewState;
        if(OnGameStateChange != null)
                OnGameStateChange(_NewState);
    }
}
