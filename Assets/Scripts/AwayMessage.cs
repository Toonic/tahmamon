﻿using UnityEngine;
using System.Collections;
using TMPro;

public class AwayMessage : MonoBehaviour {

    public static AwayMessage away;

    public AwayMessageUI UI;

    void Awake() {
        away = this;
    }

    public void Popup(string _AmmountEarned) {
        //Closes the EVO panel encase its open.
        if (EvolutionMenu.evoMenu.UI.Holder.activeInHierarchy) {
            EvolutionMenu.evoMenu.OpenEvoMenu();
        }

        UI.Ammount.text = _AmmountEarned;
        UI.Holder.SetActive(true);
    }

    public void Close() {
        UI.Holder.SetActive(false);
    }

}

[System.Serializable]
public class AwayMessageUI {
    public GameObject Holder;
    public TextMeshPro Ammount;
}

