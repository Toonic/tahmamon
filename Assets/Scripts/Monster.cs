﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Monster : MonoBehaviour {
    public double BaseLife;
    public float Level;
    public double MaxHP;
    public double CurrentHP;
    public bool Boss;
    public static Monster activeMonster;

    private tk2dSprite MonsterSprite;

    [Header("UI Elements")]
    public MonsterUI UI;

    private bool KillTimerStarted = false;

    void Awake() {
        MonsterSprite = GetComponent<tk2dSprite>();
        activeMonster = this;
    }

    IEnumerator Start() {
        if (PlayerPrefs.HasKey("CurrentLevel"))
            Level = PlayerPrefs.GetInt("CurrentLevel");

        CalculateCurrentHP();
        yield return null;
        CalculateAwayEXPEarned();
    }

    //Ripped from Clicker Heroes
    //Round Up(MAX([Is Boss] * 10, 1) * [Base Life]  * (1.6 ^ ([Level] - 1)) + ([Level] - 1) * 10, 0)
    private void CalculateCurrentHP() {
        if (KillTimerStarted) {
            KillTimerStarted = false;
            StopCoroutine("KillTimer");
        }
        CurrentHP = BaseLife * (Mathf.Pow(1.6f, (Level - 1f))) + (Level - 1) * 10;
        if (Boss) {
            CurrentHP *= 10;
        }
        MaxHP = CurrentHP;
    }

    public void CalculateAwayEXPEarned() {

        Debug.Log("Project DPS From Away: " + ProjectedDPS.dps.ProjectedDPSAmmount);

        double EXPEarnedWhileAway = 0;

        for (int x = 0; x < TimeAway.LengthAway; x++) {
            CurrentHP -= ProjectedDPS.dps.ProjectedDPSAmmount;
            if (CurrentHP <= 0) {
                EXPEarnedWhileAway += MaxHP;
                CurrentHP = MaxHP;
            }
        }

        EXP.AddEXP(EXPEarnedWhileAway);

        //AwayMessage.away.Popup(EXPEarnedWhileAway.ToString("N2"));
        AwayMessage.away.Popup(NumberToTextConverter.ConvertToString((float)EXPEarnedWhileAway));

        Debug.Log("EXP Earned While Away: " + EXPEarnedWhileAway);
    }

    public void DealDamage(double _DPS) {

        _DPS *= Achievements.achievements.GlobalAchievementMultiplyer;

        if (!KillTimerStarted)
            StartCoroutine("KillTimer");
        CurrentHP -= _DPS;
        //UI.ShakeObject.transform.DOShakePosition(0.2f, new Vector3(0.1f, 0.1f, 0), 10, 90, false).OnComplete(() => UI.ShakeObject.transform.position = new Vector3(0,0,-1));
        if(CurrentHP <= 0) {
            EXP.AddEXP(MaxHP);
            Level++;
            MonsterSprite.spriteId = Random.Range(0, 8);
            CalculateCurrentHP();
            if((ProjectedDPS.dps.ProjectedDPSAmmount * 10) < (MaxHP * 1.3f)) {
                Level--;
                if (Level == 0)
                    Level = 1;

                PlayerPrefs.SetInt("CurrentLevel", int.Parse(Level.ToString()));

                CalculateCurrentHP();
            }
        }
    }

    private IEnumerator KillTimer() {
        KillTimerStarted = true;
        yield return new WaitForSeconds(10);
        Level--;
        CalculateCurrentHP();
    }

    public void Reset() {
        Level = 1;
        PlayerPrefs.SetInt("CurrentLevel", int.Parse(Level.ToString()));
        CalculateCurrentHP();
    }
}

[System.Serializable]
public class MonsterUI {
    public GameObject ShakeObject;
}
