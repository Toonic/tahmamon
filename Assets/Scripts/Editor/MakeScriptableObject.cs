﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MakeScriptableObject : MonoBehaviour {
    [MenuItem("Assets/Create/BuildNumber")]
    public static void CreateBuild() {
        BuildNumber asset = ScriptableObject.CreateInstance<BuildNumber>();
        AssetDatabase.CreateAsset(asset, "Assets/BuildNumber.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}
