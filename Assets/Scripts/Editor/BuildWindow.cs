﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;

class BuildWindow : EditorWindow {
    private BuildNumber bn;

    public static BuildWindow bw;

    void Awake() {
        bw = this;
        bn = Resources.Load("BuildNumber") as BuildNumber;
    }

    [MenuItem("Window/Build Window")]
    public static void ShowWindow() {
        //bn = Resources.Load("BuildNumber") as BuildNumber;
        EditorWindow.GetWindow(typeof(BuildWindow));
    }

    void OnGUI() {

        float Major = -1;
        float Minor = -1;
        float Bug = -1;

        Major = float.Parse(EditorGUILayout.TextField("Major", bn.Major.ToString()));
        Minor = float.Parse(EditorGUILayout.TextField("Minor", bn.Minor.ToString()));
        Bug = float.Parse(EditorGUILayout.TextField("Bug", bn.Bug.ToString()));

        if (Major != bn.Major) {
            bn.DoStuff(Major, bn.Minor, bn.Bug);
        }

        if (Minor != bn.Minor) {
            bn.DoStuff(bn.Major, Minor, bn.Bug);
        }

        if (Bug != bn.Bug) {
            bn.DoStuff(bn.Major, bn.Minor, Bug);
        }

        if (GUILayout.Button("Major")) {
            bn.Major += 1;
            bn.Minor = 0;
            bn.Bug = 0;
            bn.DoStuff(bn.Major, bn.Minor, bn.Bug);
        }

        if (GUILayout.Button("Minor")) {
            bn.Bug = 0;
            bn.Minor += 1;
            bn.DoStuff(bn.Major, bn.Minor, bn.Bug);
        }

        if (GUILayout.Button("Bug")) {
            bn.Bug += 1;
            bn.DoStuff(bn.Major, bn.Minor, bn.Bug);
        }
    }

    [PostProcessBuildAttribute(1)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {

        BuildNumber bn = Resources.Load("BuildNumber") as BuildNumber;

        bn.Bug += 1;
        bn.DoStuff(bn.Major, bn.Minor, bn.Bug);

        Debug.Log("Auto Incremented Bug Number");
    }
}
