﻿using UnityEngine;
using System.Collections;

public class PlayerPrefConfigs : MonoBehaviour {
    [ContextMenu("Reset Player Prefs")]
    public void ClearPlayerPrefs() {
        PlayerPrefs.DeleteAll();
    }
}
