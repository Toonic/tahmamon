﻿using UnityEngine;
using System.Collections;
using TMPro;

public class ProjectedDPS : MonoBehaviour {

    public Idler[] AllIdlers;
    public ClickToAttack ClickAttacker;
    public TextMeshPro ProjectedDPSText;
    public double ProjectedDPSAmmount;

    public static ProjectedDPS dps;

    void Awake() {
        dps = this;
    }

	// Use this for initialization
	void Start () {
	    AllIdlers = FindObjectsOfType<Idler>();
        ClickAttacker = FindObjectOfType<ClickToAttack>();
	}
	
	// Update is called once per frame
	void Update () {
        float TotalIncomeRate = 0;
	    for(int x = 0; x < AllIdlers.Length; x++){
            TotalIncomeRate += AllIdlers[x].DPSRate;
        }

        TotalIncomeRate += ClickAttacker.IncomeRate * ClickAttacker.tapsPerSecond;

        //TotalIncomeRate *= Achievements.achievements.GlobalAchievementMultiplyer;

        ProjectedDPSAmmount = TotalIncomeRate;


        ProjectedDPSText.text = NumberToTextConverter.ConvertToString(TotalIncomeRate);
	}
}
