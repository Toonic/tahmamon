﻿using UnityEngine;
using System.Collections;
using TMPro;

public class ChangeBuyAmmount : MonoBehaviour {
    
    public TextMeshPro BuyAmmountText;
    
    void Start() {
        StateMachine.ChangeBuyState(BuyState.Buy1);
        BuyAmmountText.text = "Buy 1";
    }

    public void ChangeAmmount(){
        if(StateMachine.buyState == BuyState.Buy1){
            StateMachine.ChangeBuyState(BuyState.Buy10);
            BuyAmmountText.text = "Buy 10";
        }
        else if(StateMachine.buyState == BuyState.Buy10){
            StateMachine.ChangeBuyState(BuyState.Buy100);
            BuyAmmountText.text = "Buy 100";
        }
        else if(StateMachine.buyState == BuyState.Buy100){
            StateMachine.ChangeBuyState(BuyState.BuyMax);
            BuyAmmountText.text = "Buy Max";
        }
        else if(StateMachine.buyState == BuyState.BuyMax){
            StateMachine.ChangeBuyState(BuyState.Buy1);
            BuyAmmountText.text = "Buy 1";
        }
    } 
    
}
