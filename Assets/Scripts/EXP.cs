﻿using UnityEngine;
using System.Collections;
using TMPro;
using System;

public class EXP : MonoBehaviour {
    private static EXP exp;

    public static double CurrentEXP;

    public EXPUI UI;

    void Awake() {
        exp = this;
        if (PlayerPrefs.HasKey("EXPHad")) {
            CurrentEXP = Convert.ToDouble(PlayerPrefs.GetString("EXPHad"));
        }
        else {
            CurrentEXP = 15;
        }
        UpdateUI();
    }

    public static void AddEXP(double _Change) {
        CurrentEXP += _Change;
        //Debug.Log("Earned EXP: " + _Change);
        PlayerPrefs.SetString("EXPHad", CurrentEXP.ToString());
        EvolutionEXP.exp.AddToLifeTimeEXP((float)_Change);
        exp.UpdateUI();
    }

    public static void SubtractEXP(double _Change) {
        CurrentEXP -= _Change;
        PlayerPrefs.SetString("EXPHad", CurrentEXP.ToString());
        exp.UpdateUI();
    }

    private void UpdateUI() {
        UI.EXP.text = NumberToTextConverter.ConvertToString((float)CurrentEXP);
    }

    public static void Reset() {
        SubtractEXP(CurrentEXP);
    }
}

[System.Serializable]
public class EXPUI {
    public TextMeshPro EXP;
}
