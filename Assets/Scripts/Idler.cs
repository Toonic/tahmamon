﻿using UnityEngine;
using System.Collections;
using TMPro;
using System;

public class Idler : MonoBehaviour {

    //Price=BaseCost×Multiplier(#Owned);
#region Public Variables
    [Header("Settings")]
    public string Name;
    public float BaseCost;
    public float Multiplier = 1.07f; //Set somewhere between 1.07 and 1.15;
    public int NumberOwned = 1;
    public float BaseDPSRate; //Earned Per Second

    [Header("Public Stats")]
    public float BuyOnePrice;
    public float CurrentBuyPrice;
    public float DPSRate; //Earned per second.
    public float AmmountEarned = 0;
    public float AchievementMultiplyer = 1;

    [Header("Achievements")]
    public Achievement[] IdlerAchievements;

    [Header("UI Elements")]
    public IdleUIObjects UI;
#endregion

    private int AmmountToBuy;
    
    //Caled before Start
    void Awake(){
        StateMachine.OnBuyStateChanged += UpdateBuyAmmount;
    }

    IEnumerator Start() {
        LoadPlayerDataThenUpdateBuy();
        StartCoroutine(IdleMechanim());
        yield return null;
    }

    void Update() {
        if ((EXP.CurrentEXP - CurrentBuyPrice) >= 0) {
            UI.Button.enabled = true;
            UI.Button.sprite.color = UI.BuyButtonEnabledColor;
        }
        else {
            UI.Button.enabled = false;
            UI.Button.sprite.color = UI.BuyButtonDisableColor;
        }
    }
    
    private void UpdateBuyAmmount(BuyState _State){
        if(_State == BuyState.Buy1){ AmmountToBuy = 1; }
        if(_State == BuyState.Buy10){ AmmountToBuy = 10; }
        if(_State == BuyState.Buy100){ AmmountToBuy = 100; }

        CurrentBuyPrice = 0;

        BuyOnePrice = BaseCost * (Mathf.Pow(Multiplier, NumberOwned));
        for (int x = NumberOwned; x < NumberOwned + AmmountToBuy; x++) {
            CurrentBuyPrice += BaseCost * Mathf.Pow(Multiplier, x);
        }

        if(NumberOwned == 0)
            DPSRate = 0;
        else {
            DPSRate = ((BaseDPSRate * (NumberOwned + 5)) * AchievementMultiplyer) * Achievements.achievements.GlobalAchievementMultiplyer * EvolutionEXP.exp.EvoEXPMultiplier;
        }

        UpdateUI();
    }

    //Does what it says
    private void LoadPlayerDataThenUpdateBuy() {
        if (PlayerPrefs.HasKey(Name + "Owned")) {
            NumberOwned = PlayerPrefs.GetInt(Name + "Owned");
        }

        CheckAllAchievements(false);
        Achievements.achievements.CheckGlobalAchievements(false);

        UpdateBuyAmmount(StateMachine.buyState);
    }

    //The actual Idle+Score incrementation part
    private IEnumerator IdleMechanim() {
        while (true) {
            yield return new WaitForSeconds(1f);

            if(DPSRate != 0)
                Monster.activeMonster.DealDamage(DPSRate);
            AmmountEarned += DPSRate;
        }
    }

    //Updates the UI Elements as well as the Current Buy Price
    private void UpdateUI() {
        UI.Name.text = Name;
        //UI.Cost.text = CurrentBuyPrice.ToString("N2");
        UI.Cost.text = NumberToTextConverter.ConvertToString(CurrentBuyPrice);
        UI.AmmountOwned.text = NumberOwned.ToString();

        NumberToTextConverter.ConvertToString(CurrentBuyPrice);
        
        if(StateMachine.buyState == BuyState.BuyMax){ UI.BuyButtonAmmount.text = "Buy Max"; }
        else { UI.BuyButtonAmmount.text = "Buy " + AmmountToBuy; }
    }
    
    public void BuyButton(){
        if(StateMachine.buyState == BuyState.BuyMax){
            Buy(true);
        }
        else{
            Buy();
        }
    }
    
    private void Buy(bool _Max = false) { 
        if ((EXP.CurrentEXP - CurrentBuyPrice) >= 0) {
            EXP.SubtractEXP(CurrentBuyPrice);
            NumberOwned += AmmountToBuy;
            PlayerPrefs.SetInt(Name + "Owned", NumberOwned);

            CheckAllAchievements(true);
            Achievements.achievements.CheckGlobalAchievements(true);

            UpdateBuyAmmount(StateMachine.buyState);
        }
    }

    private void CheckAllAchievements(bool _VisualizeAchievement) {

        AchievementMultiplyer = 0;

        for(int x = 0; x < IdlerAchievements.Length; x++) {
            bool AchievementChecked = false;
            if (!IdlerAchievements[x].Completed)
                AchievementChecked = true;

            AchievementMultiplyer += IdlerAchievements[x].CheckAchievement(NumberOwned);

            if(AchievementChecked && IdlerAchievements[x].Completed && _VisualizeAchievement) {
                AchievementMessage.achievementMessage.GiveAchievement(IdlerAchievements[x].AchievementName + " " + Name + "s!");
            }
        }

        if (AchievementMultiplyer == 0)
            AchievementMultiplyer = 1;
    }

    public void Reset() {

        NumberOwned = 0;

        PlayerPrefs.SetInt(Name + "Owned", NumberOwned);

        CheckAllAchievements(false);

        for(int x = 0; x < IdlerAchievements.Length; x++) {
            IdlerAchievements[x].Completed = false;
        }

        Achievements.achievements.CheckGlobalAchievements(true);

        UpdateBuyAmmount(StateMachine.buyState);
    }
}

[System.Serializable]
public class IdleUIObjects {
    public TextMeshPro Name;
    public TextMeshPro Cost;
    public TextMeshPro AmmountOwned;
    public tk2dButton Button;
    public TextMeshPro BuyButtonAmmount;
    public Color BuyButtonEnabledColor;
    public Color BuyButtonDisableColor;
}
