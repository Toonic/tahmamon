﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class TimeHandler : MonoBehaviour{

    public static TimeHandler time;

    public DateTime ServerTime;

    public DateTime LastKnownTime;

    public float Timeout;

    private SNTPClient client;

    public string[] IPSToTry;
    private int currentIP = 0;

    public static double Difference;

    IEnumerator Start() {
        client = GetComponent<SNTPClient>();
        client.TimeServer = IPSToTry[0];

        yield return StartCoroutine(SetDifference());
    }

    private IEnumerator SetDifference() {
        client.AttemptRetry();

        while (!client.ReceivedTime && !client.TimeOutHappened) {
            yield return new WaitForSeconds(1.0f);

            Debug.Log(client.TimeOutHappened + "-" + client.ReceivedTime);
        }

        if (client.TimeOutHappened) {
            Debug.Log("Caught Timeout");
            currentIP++;
            if (currentIP == IPSToTry.Length) {
                Debug.Log("Failed tried after " + currentIP + "attempts.");
            }
            else {
                client.TimeServer = IPSToTry[currentIP];
                client.AttemptRetry();
                StartCoroutine(SetDifference());
                yield break;
            }
        }
        else {
            ServerTime = client.TransmitTimestamp;
            TimeSpan Diff = ServerTime - DateTime.Now;
            Difference = Diff.TotalSeconds;
            LastKnownTime = DateTime.Now;
        }
        yield return null;
    }

    private void SetTimeDifference() {
        StartCoroutine(SetDifference());
    }

    public static bool CheckTimeDesync() {
        if (time.LastKnownTime.Subtract(DateTime.Now).TotalMinutes >= 5 || time.LastKnownTime.Subtract(DateTime.Now).TotalMinutes <= -5) {
            Debug.Log("!!!!!!!Time Cheater!!!!!!!!!!");
            time.SetTimeDifference();
            return true;
        }
        else {
            time.LastKnownTime = DateTime.Now;
            return false;
        }
    }
}
