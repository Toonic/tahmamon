﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BuildNumber : ScriptableObject {
    public float Major;
    public float Minor;
    public float Bug;

    public void DoStuff(float _Major, float _Minor, float _Bug) {

        Major = _Major;
        Minor = _Minor;
        Bug = _Bug;

        string versionText = "";

        versionText = Major + "." + Minor;

        if (Bug != 0) {
            versionText += "." + Bug;
#if UNITY_EDITOR
            PlayerSettings.bundleVersion = Major + "." + _Minor;
            PlayerSettings.Android.bundleVersionCode = (int)Bug;
            PlayerSettings.iOS.buildNumber = Bug + "";
            Debug.Log("Version Text:" + versionText);
            EditorUtility.SetDirty(this);
#endif

        }
        else {
            versionText += "." + Bug;
#if UNITY_EDITOR
            PlayerSettings.Android.bundleVersionCode = (int)Bug;
            PlayerSettings.iOS.buildNumber = Bug + "";
            Debug.Log("Version Text:" + versionText);
            EditorUtility.SetDirty(this);
#endif
        }


    }

    public string GetBuildNumber() {
        return Major + "." + Minor + "." + Bug;
    }
}