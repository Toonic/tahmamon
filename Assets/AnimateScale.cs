﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AnimateScale : MonoBehaviour {

    private tk2dSprite Char;
    Vector3 Scale = new Vector3(3,3,1);
    public AnimationClip Idle;

    public bool Mirror = false;

    void Start() {
        Char = GetComponent<tk2dSprite>();
        //sStartCoroutine(Animate());
    }

    void Update() {
        Char.scale = Scale;
    }

    public void ScaleUp() {
        DOTween.To(() => Scale, x => Scale = x, new Vector3(3, 3, 1), Idle.length / 2);
    }

    public void ScaleDown() {
        if(!Mirror)
            DOTween.To(() => Scale, x => Scale = x, new Vector3(4.28f, 1.54f, 1), Idle.length / 2);
        else {
            DOTween.To(() => Scale, x => Scale = x, new Vector3(-4.28f, 1.54f, 1), Idle.length / 2);
        }
    }
}
